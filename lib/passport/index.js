const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");

const { Admins } = require("../../models");

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader("authorization"),
      secretOrKey: "secret",
    },
    async (payload, done) => {
      const user = await Admins.findByPk(payload.id);

      if (user) done(null, user);
      else done(null, false);
    }
  )
);

module.exports = passport;
