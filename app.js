const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const passport = require("./lib/passport");
const authController = require("./controllers/auth");
const adminController = require("./controllers/admin");
const studentController = require("./controllers/students");

const app = express();
const port = process.env.PORT || 8082;

app.use(cors());
app.use(bodyParser.json());
app.use(passport.initialize());

app.post("/login", authController.login);

app.post("/register", authController.register);

app.get(
  "/admin",
  passport.authenticate("jwt", { session: false }),
  adminController.getAdmin
);

app.get("/students", studentController.students);

app.get("/student/:id", studentController.studentDetail);

app.delete("/student/:id", studentController.delete);

app.post("/update-student/:id", studentController.update);

app.post("/create-student", studentController.create);

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
