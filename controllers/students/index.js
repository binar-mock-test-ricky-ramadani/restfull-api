const { Op, Sequelize } = require("sequelize");
const { Students } = require("../../models");

exports.create = async (req, res) => {
  const newStudent = await Students.create(req.body);

  if (!newStudent) {
    res.status(401).json({
      status: "FAILED",
      message: "Something wrong",
    });
  }

  res.json({
    status: "SUCCESS",
    data: newStudent,
  });
};

exports.students = async (req, res) => {
  const searchName = req.query.name || "";
  const currentPage = parseInt(req.query.page) || 1;
  const limitPage = parseInt(req.query.limit) || 8;
  const end = currentPage * limitPage;

  const students = await Students.findAndCountAll({
    where: {
      name: Sequelize.where(Sequelize.fn("lower", Sequelize.col("name")), {
        [Op.like]: `%${searchName.toLowerCase()}%`,
      }),
    },
    attributes: ["id", "name", "address"],
    order: [["name", "asc"]],
    limit: limitPage,
    offset: (currentPage - 1) * limitPage,
  });

  const pagination = {};
  pagination.totals_pages = Math.ceil(students.count / limitPage);

  if (end < students.count) {
    pagination.next = currentPage + 1;
  }

  if ((currentPage - 1) * limitPage > 0) {
    pagination.prev = currentPage - 1;
  }

  res.json({
    status: "SUCCES",
    pagination,
    data: students.rows,
  });
};

exports.studentDetail = async (req, res) => {
  const id = req.params.id;

  const studentDetail = await Students.findOne({
    where: { id },
  });

  if (!studentDetail) {
    res.status(404).json({
      status: "FAILED",
      message: "wrong id",
    });
    return;
  }

  res.json({
    status: "SUCCESS",
    data: studentDetail,
  });
};

exports.delete = async (req, res) => {
  const id = req.params.id;

  try {
    await Students.destroy({
      where: { id },
    });
    res.json({
      status: "SUCCESS",
      message: "Successfully removed student",
    });
  } catch (error) {
    res.status(404).json({
      status: "FAILED",
      message: "Failed to delete student",
    });
  }
};

exports.update = async (req, res) => {
  const id = req.params.id;

  const student = await Students.findOne({
    where: { id },
  });

  if (!student) {
    res.status(404).json({
      status: "FAILED",
      message: "Wrong Student Id",
    });
    return;
  }

  await student.update(req.body);
  await student.save();

  res.json({
    status: "SUCCESS",
    data: student,
  });
};
