const { Admins } = require("../../models");

exports.getAdmin = async (req, res) => {
  const { id } = req.user;

  const admin = await Admins.findOne({
    where: { id },
    attributes: ["name", "hobby", "id"],
  });

  if (!admin) {
    res.status(404).json({
      status: "FAILED",
      message: "Authorization failed",
    });
    return;
  }

  res.json({
    status: "SUCCESS",
    data: admin,
  });
};
