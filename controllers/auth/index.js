const jwt = require("jsonwebtoken");
const { Admins } = require("../../models");

exports.login = async (req, res) => {
  const { password } = req.body;

  const admin = await Admins.findOne({
    where: { password },
    attributes: ["name", "hobby", "id"],
  });

  if (!admin) {
    res.status(404).json({
      status: "Failed",
      message: "Wrong Id",
    });
    return;
  }

  const token = jwt.sign(
    {
      id: admin.id,
    },
    "secret"
  );

  res.json({
    status: "SUCCESS",
    token,
    data: admin,
  });
};

exports.register = async (req, res) => {
  const { password } = req.body;

  const validationId = await Admins.findOne({
    where: { password },
  });

  if (validationId) {
    res.status(301).json({
      status: "FAILED",
      message: "Id already used",
    });

    return;
  }

  const newAdmin = await Admins.create(req.body);

  res.json({
    status: "SUCCESS",
    data: newAdmin,
  });
};
